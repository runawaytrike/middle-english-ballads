module.exports = function (eleventyConfig) {
	eleventyConfig.addShortcode(
		'balladLine',
		function (line, notes) {
			return `
				<div style="display: flex; margin: 0m"><p class="balladLine">${line}</p><p class="balladLine">${notes}</p></div>
            `;
		}
	);
	// eleventyConfig.addPairedShortcode('section', function (content, sectionName) {
	// 	return `
	// 		<section class="category">${sectionName}</section>
	// 			<div class="alternative">
	// 				${content}
	// 			</div>
	// 		</section>
	// 	`;
	// });
};
